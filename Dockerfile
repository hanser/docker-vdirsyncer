FROM python:3.9-slim

LABEL maintainer="Johannes Stefan <message@johannesstefan.de>"

ENV LC_ALL=C.UTF-8 LANG=C.UTF-8

RUN apt-get -y update && DEBIAN_FRONTEND=noninteractive apt-get -y upgrade && rm -rf /var/lib/apt/lists/*

RUN useradd -m -s /bin/bash vds

RUN pip3 install --no-cache-dir requests-oauthlib vdirsyncer 

USER vds

RUN mkdir -p /home/vds/.config/vdirsyncer/

COPY ./docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]
