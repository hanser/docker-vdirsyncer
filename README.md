This is a fork from [klaernie](https://github.com/klaernie/docker-vdirsyncer)

# docker-vdirsyncer
A docker container which syncs your CalDAV/CardDAV calendars/addressbooks periodically.
This docker app uses [pimutils/vdirsyncer](https://vdirsyncer.pimutils.org/) to synchronize your CalDAV/CardDAV calendars/addressbooks between two servers.

## Getting started

1. create a vdirsyncer configuration file. See file [_config.example_](https://github.com/pimutils/vdirsyncer/blob/master/config.example) and [vdirsyncer docs](https://vdirsyncer.pimutils.org/)
2. run the container

```
mkdir files status
docker run -d \
	--name=vdirsyncer \
	-v `pwd`/configfile:/home/vds/.config/vdirsyncer/config:ro \
	-v `pwd`/files:/home/vds/.vdirsyncer/files \
	-v `pwd`/status:/home/vds/.vdirsyncer/status \
	--restart always \
	hansers/docker-vdirsyncer:latest
```

Be happy! The container will synchronize your calendars/addressbooks.
